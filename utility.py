import os
import logging
try:
    import configparser
except ImportError:
    import ConfigParser as configparser
 

def create_config(path):
    current_dir = os.path.dirname(os.path.realpath(__file__))
    
    config = configparser.ConfigParser()

    config.add_section("User")
    config.set("User", "login", "")
    config.set("User", "password", "")

    config.add_section("ASM")
    config.set("ASM", "host", "127.0.0.1")
    config.set("ASM", "port", "8000")
    config.set("ASM", "token", "")
    config.set("ASM", "timeout", "60")

    config.set("Repository", "repository", "admin:admin@127.0.0.1:7896")

    with open('/'.join([current_dir, path]), "w") as config_file:
        config.write(config_file)


def get_logger(name, logging_level='info'):
    logger = logging.getLogger(name)
    ch = logging.StreamHandler()
    #fh = RotatingFileHandler('{}\\utilsNodesGenerator.log'.format(log_dir), mode='a', maxBytes=logging_backup*1024*1024, backupCount=logging_max_size, encoding=None, delay=0)

    if logging_level == 'notset':
        logger.setLevel(logging.NOTSET)
        ch.setLevel(logging.NOTSET)
    elif logging_level == 'debug':
        logger.setLevel(logging.DEBUG)
        ch.setLevel(logging.DEBUG)
    elif logging_level == 'info':
        logger.setLevel(logging.INFO)
        ch.setLevel(logging.INFO)
    elif logging_level == 'warning':
        logger.setLevel(logging.WARNING)
        ch.setLevel(logging.WARNING)
    elif logging_level == 'error':
        logger.setLevel(logging.ERROR)
        ch.setLevel(logging.ERROR)
    elif logging_level == 'critical':
        logger.setLevel(logging.CRITICAL)
        ch.setLevel(logging.CRITICAL)
    else:
        logger.setLevel(logging.INFO)
        ch.setLevel(logging.INFO)

    # create formatter
    # if logging_level == 'debug':
    #     formatter = logging.Formatter('[%(asctime)s] [{}] [%(levelname)s] [%(funcName)s] [%(lineno)s] : %(message)s'.format(
    #             os.getpid(),
    #         ), '%Y-%m-%d %H:%M:%S')
    formatter = logging.Formatter('[%(asctime)s] [%(levelname)s] : %(message)s', '%Y-%m-%d %H:%M:%S')

    ch.setFormatter(formatter)
    #fh.setFormatter(formatter)

    logger.addHandler(ch)
    #logger.addHandler(fh)

    return logger
