import re
import os
import sys
import time
import utility
import requests
import xml.dom.minidom
import xml.etree.ElementTree as ET
from asm_api import asm_api
from distutils.version import StrictVersion
try: 
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup
try:
    import configparser
except ImportError:
    import ConfigParser as configparser

# namespace for xml parser work correctly
ET.register_namespace("", "http://www.w3.org/2001")

# create logger
logger = utility.get_logger(__name__)

# check settings file exists
config_path = "settings.ini"
if not os.path.exists(config_path):
    logger.info("settings.ini not found")
    utility.create_config(config_path)
    logger.info('Default settings.ini created')
    logger.info("Configure settings.ini to work with the utility")
    sys.exit(1)

# get parameters from settings.ini
config = configparser.ConfigParser()
config.read("settings.ini")

login = config.get("User", "login")
password = config.get("User", "password")

asm_host = config.get("ASM", "host")
asm_port = config.get("ASM", "port")
asm_token = config.get("ASM", "token")
asm_timeout = config.get("ASM", "timeout")

repository_address = config.get("Repository", "repository")

clientId = config.get("OAuth2", "clientId")
clientSecret = config.get("OAuth2", "clientSecret")

# check all required parameters are set
if not all(bool(value) for value in (login, password, asm_host, asm_port,
                                    asm_token, asm_timeout, repository_address)):
    logger.info("Not all required parameters have been specified. Check the settings.ini file.")
    sys.exit(1)

# create ASM session
asm = asm_api.Handler(host=asm_host,
                      port=asm_port,
                      token=asm_token,
                      timeout=asm_timeout)

asm_path = asm.get_path()

rest_url = 'https://{}:{}@nexus3.alphaopen.com/service/rest/repository/browse/dist/'.format(login, password)
base_url = 'https://{}:{}@nexus3.alphaopen.com/repository/dist/'.format(login, password)

# -Dalpha.data.dir for server and repository
server_data_dir_key = rf'"-Dalpha.data.dir={asm_path}\var\work\server\data-dir"'
repository_data_dir_key = rf'"-Dalpha.data.dir={asm_path}\var\work\repository\data-dir"'

#get last alphalogic version
def get_last_version():
    version_list = requests.get(rest_url+'server-main')
    version_list.raise_for_status()

    html = version_list.content
    soup = BeautifulSoup(html, 'html.parser')

    data = []
    table = soup.find('table')
    rows = table.find_all('tr')
    for row in rows:
        cols = row.find_all('a')
        cols = [ele.text.strip() for ele in cols]
        elements = [ele for ele in cols if ele]
        data.append(elements)

    versions = [item[0] for item in data if item and bool(re.match("^([\d\.]+)$", item[0]))]
    last_version = sorted(versions, key=StrictVersion)[-1]

    return last_version


# register namespace same as in xml
def namespace(element):
    m = re.match(r'\{(.*)\}', element.tag)
    return m.group(1) if m else ''


# add <repository> block to server.xml
def edit_xml(component):
    # server.xml
    xml_filename = r'.\conf\server.xml'
    xml_content = asm.get_xml(component, xml_filename)

    root = ET.fromstring(xml_content)
    ET.register_namespace('', namespace(root))
    ET.register_namespace('xsi', "http://www.w3.org/2001/XMLSchema-instance")
    #xml_version = root.tag
    repository = ET.Element("repository")
    repository.text = repository_address

    root.append(repository)

    xmlstr = xml.dom.minidom.parseString(ET.tostring(root, 'utf-8')).toprettyxml()\
                                                    .replace("\n\t\n", "").replace("\n    ", "")
                                                
    asm.set_xml(component, xml_filename, xmlstr)


    # security.xml
    xml_filename = r'.\conf\security.xml'
    xml_content = asm.get_xml(component, xml_filename)

    root = ET.fromstring(xml_content)
    ET.register_namespace('', namespace(root))
    ET.register_namespace('xsi', "http://www.w3.org/2001/XMLSchema-instance")

    for item in root.findall('*'):
        if "static" in str(item):
            root.remove(item)

    oauth2 = ET.Element("oauth2")
    client = ET.SubElement(oauth2, 'client')

    authUri = ET.SubElement(client, 'authUri')
    authUri.text = "https://accounts.google.com/o/oauth2/v2/auth?scope=profile&response_type=code&state=security_token_${saltLong}&redirect_uri=${clientUri}&client_id=${clientId}&amp;access_type=offline"
    clientIdField = ET.SubElement(client, 'clientId')
    clientIdField.text = clientId
    clientSecretField = ET.SubElement(client, 'clientSecret')
    clientSecretField.text = clientSecret
    scope = ET.SubElement(client, 'scope')
    scope.text = ""
    clientUri = ET.SubElement(client, 'clientUri')
    clientUri.text = "http://localhost:9004"
    tokenUri = ET.SubElement(client, 'tokenUri')
    tokenUri.text = "https://oauth2.googleapis.com/token"
    refreshUri = ET.SubElement(client, 'refreshUri')
    refreshUri.text = "https://oauth2.googleapis.com/token"
    loginAttribute = ET.SubElement(client, 'loginAttribute')
    loginAttribute.text = "name"
    grantType = ET.SubElement(client, 'grantType')
    grantType.text = "authorization_code"
    firstNameAttribute = ET.SubElement(client, 'firstNameAttribute')
    firstNameAttribute.text = "given_name"
    lastNameAttribute = ET.SubElement(client, 'lastNameAttribute')
    lastNameAttribute.text = "family_name"
    profileUri = ET.SubElement(client, 'profileUri')
    profileUri.text = "https://www.googleapis.com/oauth2/v2/userinfo"
    defaultRole = ET.SubElement(client, 'defaultRole')
    defaultRole.text = "USER"

    # root.append(oauth2)
    root.insert(0, oauth2)
    # print(dir(root))
    # print(type(root))
    # import sys
    # sys.exit(1)


    xmlstr = xml.dom.minidom.parseString(ET.tostring(root, 'utf-8')).toprettyxml()\
                                                    .replace("\n\t\n", "").replace("\n    ", "")

    asm.set_xml(component, xml_filename, xmlstr)


### Prepare for Download
last_version = get_last_version()

version = input(f"Specify Alphalogic version [{last_version}]: ")
target_os = 'windows'
if not version:
	version = last_version
logger.info(f"Starting installation of Alphalogic {version}")

urls = {
        'server': 'server-main/{version}/server-main-{version}.zip'.format(version=version),
        'repositoryserver': 'repository/{version}/repository-{version}.zip'.format(version=version),
        'adminapplication': 'admin-main/{version}/admin-main-{version}-{target_os}.zip'.format(version=version, target_os=target_os),
        'standaloneclient': 'client-standalone-main/{version}/client-standalone-main-{version}-{target_os}.zip'.format(version=version, target_os=target_os),
        }

### Delete All Files
delete_files = ['admin-main-', 'repository-', 'server-main-', 'client-standalone-main-']
installed_components = []
downloaded_files = []

for file in asm.get_files():
    if any(delete_file in file for delete_file in delete_files) and file in downloaded_files:
        asm.delete_files(file)

### Install and Edit
for component in urls:
    download_url = base_url + urls[component]
    file_name = download_url.split("/")[-1]
    
    logger.debug(f"Check URL: {download_url}")
    check = requests.head(download_url)
    logger.debug(f"URL status code: {check.status_code}")
    check.raise_for_status()

    try:
        logger.info(f"Download: {file_name}")
        asm.download(download_url)
    except requests.exceptions.HTTPError as ex:
        retries = 1
        while retries > 4:
            logger.error(f"Failed to download {file_name}. Retry {retries}...")
            try:
                asm.download(download_url)
                break
            except:
                retries += 1
            if retries == 4:
                logger.error(f"Cant download {file_name}")
                sys.exit(1)

    asm_component = asm.install(file_name)

    installed_components.append(asm_component)
    downloaded_files.append(file_name)

### Delete All Components

for component in asm.get_task():
    if component.startswith(("server_", "adminapplication_", "repositoryserver_", "standaloneclient_")) and component not in installed_components:
        if asm.get_task(component)['status'] == 'running':
            asm.stop(component)
        logger.info(f"Delete: {component}")
        asm.delete_task(component)
    # if not component.startswith('java') and component not in installed_components:
    #     if asm.get_task(component)['status'] == 'running':
    #         asm.stop(component)
    #     logger.info(f"Delete: {component}")
    #     asm.delete_task(component)

# Create scripts and run components
for asm_component in installed_components:
    logger.info(f"Starting setup for: {asm_component}")
    if asm_component.startswith('server_'):
        logger.info(f"Set task: {asm_component}")
        asm.set_task(asm_component, 'cmdline_padding', server_data_dir_key)
        logger.info(f"Edit XMl: {asm_component}")
        edit_xml(asm_component)
    if asm_component.startswith('repositoryserver_'):
        logger.info(f"Set task: {asm_component}")
        asm.set_task(asm_component, 'cmdline_padding', repository_data_dir_key)
    if not asm_component.startswith(('adminapplication_', 'standaloneclient_')):
        for component in asm.get_task(asm_component.split('_')[0]):
            if asm.get_task(component)['status'] == 'running':
                logger.info(f"Stop task: {component}")
                asm.stop(component)
        logger.info(f"Start task: {component}")
        asm.start(asm_component)
    if asm_component.startswith(('standaloneclient_')):
        vmoptions = asm.get_component(asm_component)['vmoptions']
        vmoptions = '\n'.join([vmoptions, "-Dalpha.show.loginThrough=true", 
                                          "-Dalpha.oauth.dialog.width=600", 
                                          "-Dalpha.oauth.dialog.height=600"])

        asm.set_json(component=asm_component, key='vmoptions', value=vmoptions)
    if asm_component.startswith(('adminapplication_', 'standaloneclient_')):
        logger.info(f"Create script: {asm_component}")
        asm.create_script(asm_component)


hold_console = input(f"Sucsessfully installed Alphalogic {version}!\nPress Enter to continue...")